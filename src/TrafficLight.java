//Name: Mr. Jesse Byers
//Student Number: 11559304
//Instructor: Dr Md Anisur Rahman, PhD
//Date: 10/09/2019
//Class Name: TrafficLight.java

//Description: Creates a traffic light simulator using radio buttons to change the lights to reg, green or yellow


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.stage.Stage;
import javafx.scene.shape.*;

/**
 * Creates a traffic light simulation program
 *
 */
public class TrafficLight extends Application {
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {


        //circles
        Circle greenCircle = new Circle();
        greenCircle.setCenterX(250 / 2);
        greenCircle.setCenterY(50);
        greenCircle.setStroke(Color.BLACK);
        greenCircle.setFill(Color.WHITE);
        greenCircle.setRadius(15);

        Circle orangeCircle = new Circle();
        orangeCircle.setCenterX(250 / 2);
        orangeCircle.setCenterY(100);
        orangeCircle.setStroke(Color.BLACK);
        orangeCircle.setFill(Color.WHITE);
        orangeCircle.setRadius(15);

        Circle redCircle = new Circle();
        redCircle.setCenterX(250 / 2);
        redCircle.setCenterY(150);
        redCircle.setStroke(Color.BLACK);
        redCircle.setFill(Color.WHITE);
        redCircle.setRadius(15);

        //rectangle
        Rectangle r1 = new Rectangle(250 / 4, 15, 125, 175);
        r1.setStroke(Color.BLACK);
        r1.setFill(Color.WHITE);


        //Radio Buttons
        ToggleGroup toggleColour = new ToggleGroup();

        RadioButton redButton = new RadioButton();
        redButton.setText("Red");
        redButton.setToggleGroup(toggleColour);


        RadioButton yellowButton = new RadioButton();
        yellowButton.setText("Yellow");
        yellowButton.setToggleGroup(toggleColour);

        RadioButton greenButton = new RadioButton();
        greenButton.setText("Green");
        greenButton.setToggleGroup(toggleColour);

        HBox hBox = new HBox(redButton, yellowButton, greenButton);
        hBox.setSpacing(20);
        hBox.setLayoutY(210);
        hBox.setLayoutX(30);


        //Colour change
        redButton.setOnAction(e -> {
            if (redButton.isSelected()) {
                redCircle.setFill(Color.WHITE);
                greenCircle.setFill(Color.WHITE);
                orangeCircle.setFill(Color.WHITE);

                redCircle.setFill(Color.RED);
            }
        });

        greenButton.setOnAction(e -> {
            if (greenButton.isSelected()) {
                redCircle.setFill(Color.WHITE);
                greenCircle.setFill(Color.WHITE);
                orangeCircle.setFill(Color.WHITE);

                greenCircle.setFill(Color.GREEN);
            }
        });

        yellowButton.setOnAction(e -> {
            if (yellowButton.isSelected()) {
                redCircle.setFill(Color.WHITE);
                greenCircle.setFill(Color.WHITE);
                orangeCircle.setFill(Color.WHITE);

                orangeCircle.setFill(Color.ORANGE);
            }
        });


        // Create a scene and place a button in the scene
        Pane pane = new Pane();

        pane.getChildren().add(r1);
        pane.getChildren().add(greenCircle);
        pane.getChildren().add(orangeCircle);
        pane.getChildren().add(redCircle);
        pane.getChildren().add(hBox);


        //Set Stage
        Scene scene = new Scene(pane, 250, 250);
        primaryStage.setTitle("Traffic Light Simulation"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }


    public static void main(String[] args) {
        Application.launch(args);
    }
}
